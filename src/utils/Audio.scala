package utils

import javax.sound.sampled.AudioSystem
import javax.sound.sampled.Clip

object Audio {

  def playSound(soundPath: String): Unit = {
    val sound = new Audio(soundPath)
    sound.getClip.start()
  }

}

class Audio private(val soundPath: String) {
  private var clip: Clip = _
  try {
    val audio = AudioSystem.getAudioInputStream(getClass.getResource(soundPath))
    clip = AudioSystem.getClip
    clip.open(audio)
  } catch {
    case e: Exception => e.printStackTrace()
  }

  private def getClip: Clip = clip

}