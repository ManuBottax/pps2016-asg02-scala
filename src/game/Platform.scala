package game

import controller.Keyboard
import model.characters._
import model.objects._
import java.awt.Graphics
import java.awt.Graphics2D
import scala.swing.Image
import java.util.concurrent.CopyOnWriteArrayList
import javax.swing.JPanel

import model.characters.strategy.SuperJumpStrategy
import utils.{Audio, Res, Utils}

import scala.collection.mutable.ListBuffer


trait Background {
  var backgroundImage : Image
  var posX : Int
}

class BackgroundImpl ( image: Image, positionX : Int) extends Background {

  override var backgroundImage: Image = image
  override var posX: Int = positionX
}


class Platform() extends JPanel {

  private val endLevelPosition = 4600
  private var enemyList: List[RunnableCharacter] = _
  private var objectList: CopyOnWriteArrayList[GameElement] = _
  private var mario: Mario = _
  private var movement: Int = 0
  private var xPos: Int = 0
  private var heightLimit: Int = 0
  private var castle: Image = _
  private var start: Image = _
  private var flagImage: Image = _
  private var castleImage: Image = _
  private var floorOffsetY: Int = 0

  var background1: Background = _
  var background2: Background = _
  var score: Int = 0
  var completed: Boolean = false

  initializeBackground()
  initializeCharacter()
  createGameObjects()
  this.setFocusable(true)
  this.requestFocusInWindow
  this.addKeyListener(new Keyboard)

  private def initializeBackground() = {
    this.background1 = new BackgroundImpl (Utils.getImage(Res.IMG_BACKGROUND),Res.BACKGROUND1_X_POS)
    this.background2 = new BackgroundImpl (Utils.getImage(Res.IMG_BACKGROUND), Res.BACKGROUND2_X_POS)
    this.castle = Utils.getImage(Res.IMG_CASTLE)
    this.start = Utils.getImage(Res.START_ICON)
    this.castleImage = Utils.getImage(Res.IMG_CASTLE_FINAL)
    this.flagImage = Utils.getImage(Res.IMG_FLAG)
    this.floorOffsetY = Res.FLOOR_OFFSET_Y
  }

  private def initializeCharacter() = {
    val characterFactory = new CharacterFactoryImpl
    this.movement = Res.MOVEMENT
    this.xPos = Res.X_POS
    this.heightLimit = Res.HEIGHT_LIMIT
    this.mario = characterFactory.createMario(300, 245)
    var enemy = new ListBuffer[RunnableCharacter] ()
    enemy += characterFactory.createMushroom(800, 263)
    enemy += characterFactory.createTurtle(1000, 243)
    this.enemyList = enemy.toList
  }

  private def createGameObjects() = {
    val factory = new GameObjectFactoryImpl
    this.objectList = new CopyOnWriteArrayList[GameElement]
    for (p <- ObjectPosition.values) {
      p.getType match {
        case Res.TYPE_TUNNEL => this.objectList.add(factory.createTunnel(p.getX, p.getY))
        case Res.TYPE_BLOCK => this.objectList.add(factory.createBlock(p.getX, p.getY))
        case Res.TYPE_PIECE => this.objectList.add(factory.createPiece(p.getX, p.getY))
      }
    }

    this.objectList.add(new MushroomPowerUp(1202, 140))
  }

  def getFloorOffsetY: Int = floorOffsetY

  def setFloorOffsetY(floorOffsetY: Int): Unit = this.floorOffsetY = floorOffsetY

  def setBackground1PosX(x: Int): Unit = background1.posX = x

  def setBackground2PosX(x: Int): Unit = background2.posX = x

  def getObjectList: CopyOnWriteArrayList[GameElement] = objectList

  def getEnemyList: List[RunnableCharacter] = enemyList

  def getMovement: Int = movement

  def getXPos: Int = xPos

  def setXPos(xPos: Int): Unit = this.xPos = xPos

  def getHeightLimit: Int = heightLimit

  def setHeightLimit(heightLimit: Int): Unit = this.heightLimit = heightLimit

  def setMovement(movement: Int): Unit = this.movement = movement

  def getMario: Mario = this.mario

  def checkContact(): Unit = {
    objectList.forEach(o => {
      enemyList.foreach(e => {
        if (e.isNearby(o)) e.contact(o)
        if (e.isNearby(o)) e.contact(o)
      })

      if (this.mario.isNearby(o)) this.mario.contact(o)
      o match {
        case p: Piece =>
          if (this.mario.contactPiece(p)) {
            this.score = this.score + 20
            Audio.playSound(Res.AUDIO_MONEY)
            this.objectList.remove(p)
          }

        case m: MushroomPowerUp =>
          if (this.mario.contactPowerUp(m)) {
            Audio.playSound(Res.AUDIO_POWERUP)
            this.mario.powerUp = this.mario.powerUp + 1
            this.mario.jumpingStrategy = new SuperJumpStrategy
            this.objectList.remove(m)
          }

        case _ => Unit
      }
    })

      enemyList.foreach(e => {
      var i = 0
      while ( i < enemyList.size ) {
        if (i != enemyList.indexOf(e)) {
          val target = enemyList(i)
          if (e.isNearby(target)) e.contact(enemyList(i))
            if (this.mario.isNearby(target)) {
              this.mario.hitEnemy(target)
            }
        }
          i += 1; i - 1
      }
    })
  }

  def moveFixedObject(): Unit =
    this.xPos match {
      case i if  i >= 0 && i <= endLevelPosition => objectList.forEach(o => o.move())
      case j if j > endLevelPosition => this.completed = true
      case _ => Unit
  }

  def moveEnemy(): Unit = enemyList.foreach(e => e.move() )

  private def updateBackgroundOnMovement() = {
    this.xPos match {
      case i if  i >= 0 && i <= endLevelPosition =>
        this.setXPos(this.getXPos + this.getMovement)
        this.background1.posX = this.background1.posX - this.getMovement
        this.background2.posX = this.background2.posX - this.getMovement

      case j if j > endLevelPosition => this.completed = true
      case _ => Unit
    }

    val back1position = this.background1.posX
    back1position match {
      case -800 => this.background1.posX = 800
      case 800 => this.background1.posX = -800
      case _ => Unit
    }

    val back2position = this.background2.posX
    back2position match {
      case -800 => this.background2.posX = 800
      case 800 => this.background2.posX = -800
      case _ => Unit
    }
  }

  override def paintComponent(g: Graphics): Unit = {
    super.paintComponent(g)
    val g2 = g.asInstanceOf[Graphics2D]

    this.checkContact()
    this.updateBackgroundOnMovement()
    this.moveFixedObject()
    this.moveEnemy()

    g2.drawImage(this.background1.backgroundImage, this.background1.posX, 0, null)
    g2.drawImage(this.background2.backgroundImage, this.background2.posX, 0, null)
    g2.drawImage(this.castle, 10 - this.getXPos, 95, null)
    g2.drawImage(this.start, 220 - this.getXPos, 234, null)
    this.getObjectList.forEach( o => g2.drawImage(o.getObjectImage, o.getX, o.getY, null))
    g2.drawImage(this.flagImage, Res.FLAG_X_POS - this.getXPos, Res.FLAG_Y_POS, null)
    g2.drawImage(this.castleImage, Res.CASTLE_X_POS - this.getXPos, Res.CASTLE_Y_POS, null)
    if (this.getMario.isJumping)
      g2.drawImage(this.getMario.jump, this.getMario.getX, this.getMario.getY, null)
    else
      g2.drawImage(this.getMario.walk(Res.IMGP_CHARACTER_MARIO, Res.MARIO_FREQUENCY), this.getMario.getX, this.getMario.getY, null)
    this.getEnemyList.foreach( enemy => {
      enemy match {
        case m: Mushroom => if (m.isAlive) g2.drawImage(m.walk(Res.IMGP_CHARACTER_MUSHROOM, Res.MUSHROOM_FREQUENCY), m.getX, m.getY, null)
                            else g2.drawImage(m.getDeadImage, m.getX, m.getY + Res.MUSHROOM_DEAD_OFFSET_Y, null)
        case t: Turtle =>  if (t.isAlive) g2.drawImage(t.walk(Res.IMGP_CHARACTER_TURTLE, Res.TURTLE_FREQUENCY), t.getX, t.getY, null)
                           else g2.drawImage(t.getDeadImage, t.getX, t.getY + Res.TURTLE_DEAD_OFFSET_Y, null)
      }
    })
  }
}