package game

import utils.Audio
import utils.Res
import javax.swing.{JFrame, JOptionPane}
import java.awt.event.WindowEvent

object Main {
  private[this] val windowsWidth: Int = 700
  private[this] val windowsHeight: Int = 360
  private[this] val windowsTitle = "Super Mario 3.0"
  private[this] val pause = 3
  private[this] val pauseAfterDeath = 6000
  var scene: Platform = _

  def getScene: Platform = scene

  def main(args: Array[String]): Unit = {

    val frame = new JFrame(windowsTitle)
    val thread = new Thread {
      override def run(): Unit = {
        while (true) {
          if (Main.scene.getMario.isAlive && !Main.scene.completed) {
            Main.scene.repaint()
            try Thread.sleep(pause)
            catch {
              case e: InterruptedException => e.printStackTrace()
            }
          }
          else if (!Main.scene.getMario.isAlive) {
            try {
              val thread = new Thread(() => {
                Audio.playSound(Res.AUDIO_DEATH)
                Thread.sleep(pauseAfterDeath)
                })
              thread.start()
              JOptionPane.showMessageDialog(frame, "\t\t\tGame Over!\n \tYour Score: " + scene.score)

            } catch {
              case e: InterruptedException => e.printStackTrace()
            }
            Thread.currentThread.interrupt()
            frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING))
          }
          else if (Main.scene.completed) {
            try {
              val thread = new Thread(() => {
                Audio.playSound(Res.AUDIO_STAGE_CLEAR)
                Thread.sleep(pauseAfterDeath)
              })
              thread.start()
              scene.score = scene.score + 1000
              JOptionPane.showMessageDialog(frame, "\t\t\tLevel Completed!\n \tYour Score: " + scene.score)
            } catch {
              case e: InterruptedException => e.printStackTrace()
            }
            Thread.currentThread.interrupt()
            frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING))
          }
        }
      }
    }

    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
    frame.setSize(windowsWidth, windowsHeight)
    frame.setLocationRelativeTo(null)
    frame.setResizable(false)
    frame.setAlwaysOnTop(true)
    Main.scene = new Platform()
    frame.setContentPane(Main.scene)
    frame.setVisible(true)

    thread.start()
  }
}
