package model.characters

import java.awt.Image
import utils._

object MushroomParameter {
  val WIDTH: Int = 27
  val HEIGHT: Int  = 30
}

class Mushroom( x: Int,  y: Int) extends RunnableCharacter(x, y, MushroomParameter.WIDTH, MushroomParameter.HEIGHT) with Runnable {
  this.setCharacterImage(Utils.getImage(Res.IMG_MUSHROOM_DEFAULT))
  val mushroom = new Thread(this)
  mushroom.start()

  override def getDeadImage: Image = Utils.getImage(if (this.isRightDirection) Res.IMG_MUSHROOM_DEAD_DX else Res.IMG_MUSHROOM_DEAD_SX)
}

object Mushroom {
  def apply (x:Int , y: Int): Mushroom = new Mushroom (x,y)
}