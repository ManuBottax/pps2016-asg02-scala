package model.characters

trait CharacterFactory {
  def createMushroom(xPosition: Int, yPosition: Int): RunnableCharacter

  def createTurtle(xPosition: Int, yPosition: Int): RunnableCharacter

  def createMario(xPosition: Int, yPosition: Int): Mario
}