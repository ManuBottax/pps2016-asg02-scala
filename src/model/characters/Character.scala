package model.characters

import model.GameObject
import scala.swing.Image

trait Character extends GameObject {
  def walk(name: String, frequency: Int): Image

  def getCounter: Int

  var isAlive: Boolean

  var isMoving: Boolean

  def isRightDirection: Boolean

  def isNearby(obj: GameObject): Boolean
}