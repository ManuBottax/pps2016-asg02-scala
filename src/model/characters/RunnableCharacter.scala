package model.characters

import model.GameObject
import model.characters.strategy.HitStrategy
import scala.swing.Image

abstract class RunnableCharacter( x: Int,  y: Int,  width: Int,  height: Int) extends BasicCharacter(x, y, width, height) with Runnable {

  private val pause = 15
  private var characterImage: Image = _
  private var characterDirection: Int = 0

  this.characterDirection = BasicCharacterParameter.RIGHT_DIRECTION
  this.setMoving(true)

  def setCharacterImage(image: Image): Unit = this.characterImage = image

  override def move(): Unit = {
    this.characterDirection = if (isRightDirection) BasicCharacterParameter.RIGHT_DIRECTION
    else BasicCharacterParameter.LEFT_DIRECTION
    super.setX(super.getX + this.characterDirection)
  }

  override def run(): Unit =
    while (true)
      if (this.isAlive) {
        this.move()
        try
          Thread.sleep(pause)
        catch { case e: InterruptedException => e.printStackTrace() }
      }

  def contact(o: GameObject): Unit =
    if (this.hit(o, HitStrategy.AHEAD) && this.isRightDirection) {
      this.setRightDirection(false)
      this.characterDirection = BasicCharacterParameter.LEFT_DIRECTION
    }
    else if (this.hit(o, HitStrategy.BACK) && !this.isRightDirection) {
      this.setRightDirection(true)
      this.characterDirection = BasicCharacterParameter.RIGHT_DIRECTION
    }

  def getDeadImage: Image
}