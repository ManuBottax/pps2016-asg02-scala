package model.characters

import scala.swing.Image
import java.lang._

import game.Main
import model.characters.strategy.{ClassicJumpStrategy, HitStrategy, JumpingStrategy}
import model.objects.{GameElement, MushroomPowerUp, Piece}
import utils._

object MarioParameter {
  val MARIO_OFFSET_Y_INITIAL = 243
  val FLOOR_OFFSET_Y_INITIAL = 293
  val WIDTH = 28
  val HEIGHT = 50
}

class Mario(x: Int, y: Int) extends BasicCharacter(x, y, MarioParameter.WIDTH, MarioParameter.HEIGHT) {

  val marioImage: Image = Utils.getImage(Res.IMG_MARIO_DEFAULT)

  var jumping: Boolean = false
  var jumpingStrategy: JumpingStrategy = new ClassicJumpStrategy
  var powerUp: Int = 0

  def isJumping: Boolean = jumping

  def setJumping(jumping: Boolean): Unit = this.jumping = jumping

  private def performJump() = jumpingStrategy.performJump(this)

  def jump: Image = {

    performJump()

    powerUp match {
      case n if n > 0 =>
        if (this.isRightDirection)
          Utils.getImage(Res.IMG_SUPER_MARIO_SUPER_DX)
        else
          Utils.getImage(Res.IMG_SUPER_MARIO_SUPER_SX)

      case 0 =>
        if (this.isRightDirection)
          Utils.getImage(Res.IMG_MARIO_SUPER_DX)
        else
          Utils.getImage(Res.IMG_MARIO_SUPER_SX)

    }
  }

  override def walk(name: String, frequency: Int): Image =   {

    powerUp match {
      case n if n > 0 =>
        Utils.getImage(Res.IMG_BASE + "S" + Res.IMGP_CHARACTER_MARIO + (if (!this.isMoving || {
            this.counter += 1; this.counter
            } % frequency == 0) Res.IMGP_STATUS_ACTIVE
            else Res.IMGP_STATUS_NORMAL) + (if (this.isRightDirection) Res.IMGP_DIRECTION_DX
            else Res.IMGP_DIRECTION_SX) + Res.IMG_EXT)
      case 0 => super.walk(name, frequency)

  }
  }

  def contact(elem: GameElement): Unit = {
    if (this.hit(elem, HitStrategy.AHEAD) && this.isRightDirection || this.hit(elem, HitStrategy.BACK) && !this.isRightDirection) {
      Main.getScene.setMovement(0)
      this.setMoving(false)
    }
    if (this.hit(elem, HitStrategy.BELOW) && this.jumping) Main.getScene.setFloorOffsetY(elem.getY)
    else {
      if (this.hit(elem, HitStrategy.BELOW)) {}
      else {
        Main.getScene.setFloorOffsetY(MarioParameter.FLOOR_OFFSET_Y_INITIAL)
        if (!this.jumping) this.setY(MarioParameter.MARIO_OFFSET_Y_INITIAL)
      }
      if (this.hit(elem, HitStrategy.ABOVE)) Main.getScene.setHeightLimit(elem.getY + elem.getHeight)
      else if (!this.hit(elem, HitStrategy.ABOVE) && !this.jumping) Main.getScene.setHeightLimit(0)
    }
  }

  def hitEnemy(character: BasicCharacter): Unit = {

    if (hit(character, HitStrategy.AHEAD) || hit(character, HitStrategy.BACK))
      if (character.isAlive) {
        powerUp match {
          case n if n > 1 =>
            Audio.playSound(Res.AUDIO_POWERDOWN)
            this.powerUp = this.powerUp - 1
            character.setAlive(false)
          case 1 =>
            Audio.playSound(Res.AUDIO_POWERDOWN)
            this.powerUp = this.powerUp - 1
            this.jumpingStrategy = new ClassicJumpStrategy
            character.setAlive(false)
          case 0 =>
            super.setMoving(false)
            super.setAlive(false)

          case _ => Unit
        }

      }
      else super.setAlive(true)
    else if (hit(character, HitStrategy.BELOW)) {
      character.setMoving(false)
      character.setAlive(false)
    }

  }

  def contactPiece(piece: GameElement): Boolean =
    if (piece.isInstanceOf[Piece])
      (this.hit(piece, HitStrategy.BACK) || this.hit(piece, HitStrategy.ABOVE) || this.hit(piece, HitStrategy.AHEAD)) ||
       this.hit(piece, HitStrategy.BELOW)
    else
      false


  def contactPowerUp(powerUp: MushroomPowerUp): Boolean =
    if (powerUp.isInstanceOf[MushroomPowerUp])
      (this.hit(powerUp, HitStrategy.BACK) || this.hit(powerUp, HitStrategy.ABOVE) || this.hit(powerUp, HitStrategy.AHEAD)) ||
        this.hit(powerUp, HitStrategy.BELOW)
    else
      false
}

object Mario {
  def apply (x:Int , y: Int): Mario = new Mario (x,y)
}