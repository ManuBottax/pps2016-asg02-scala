package model.characters

import java.awt.Image
import game.Main
import game.Platform
import model.GameObject
import model.characters.strategy.ClassicHitStrategy
import model.characters.strategy.HitStrategy
import utils.Res
import utils.Utils

object BasicCharacterParameter {
  val RIGHT_DIRECTION: Int  = 1
  val LEFT_DIRECTION: Int = -1
  val PROXIMITY_MARGIN: Int = 10
}

class BasicCharacter(var x: Int, var y: Int, var width: Int, var height: Int) extends Character {

  override var isAlive: Boolean = true
  override var isMoving: Boolean = _

  var counter = 0
  var hitStrategy: HitStrategy = new ClassicHitStrategy()
  var isRightDirection: Boolean = true

  override def getX: Int = x

  override def getY: Int = y

  override def getWidth: Int = width

  override def getHeight: Int = height

  override def getCounter: Int = counter

  override def walk(name: String, frequency: Int): Image = {
    Utils.getImage( Res.IMG_BASE + name + (if (!this.isMoving || {this.counter += 1; this.counter} % frequency == 0)
      Res.IMGP_STATUS_ACTIVE
    else
      Res.IMGP_STATUS_NORMAL) + (if (this.isRightDirection) Res.IMGP_DIRECTION_DX else Res.IMGP_DIRECTION_SX) + Res.IMG_EXT

  }

  override def move(): Unit = if (Main.getScene.getXPos >= 0) this.x = this.x - Main.getScene.getMovement

  override def isNearby(o: GameObject): Boolean = (this.x > o.getX - BasicCharacterParameter.PROXIMITY_MARGIN && this.x < o.getX + o.getWidth + BasicCharacterParameter.PROXIMITY_MARGIN) ||
    (this.getX + this.width > o.getX - BasicCharacterParameter.PROXIMITY_MARGIN && this.x + this.width < o.getX + o.getWidth + BasicCharacterParameter.PROXIMITY_MARGIN)

  def setRightDirection(direction: Boolean): Unit = isRightDirection = direction

  def setMoving(moving: Boolean): Unit = isMoving = moving

  def setAlive(alive: Boolean): Unit = isAlive = alive

  def setX(x: Int): Unit = this.x = x

  def setY(y: Int): Unit = this.y = y

  def setCounter(counter: Int): Unit = this.counter = counter

  def hit(o: GameObject, direction: HitStrategy.HitDirection): Boolean = hitStrategy.hit(this, o, direction)

}