package model.characters

import java.awt.Image
import utils._

object TurtleParameter {
  val WIDTH: Int = 43
  val HEIGHT: Int = 50
}

class Turtle( x: Int, y: Int) extends RunnableCharacter(x, y, TurtleParameter.WIDTH, TurtleParameter.HEIGHT) {
  this.setCharacterImage(Utils.getImage(Res.IMG_TURTLE_IDLE))
  new Thread(this).start()

  override def getDeadImage: Image = Utils.getImage(Res.IMG_TURTLE_DEAD)
}

object Turtle {
  def apply (x:Int , y: Int): Turtle = new Turtle (x,y)
}