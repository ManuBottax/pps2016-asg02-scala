package model.characters.strategy

import model.GameObject
import model.characters.Character

class ClassicHitStrategy extends HitStrategy {
  def hit(target: Character, o: GameObject, direction: HitStrategy.HitDirection): Boolean = {

    direction match {
      case HitStrategy.AHEAD =>
        if (target.isRightDirection ) {
          !(target.getX + target.getWidth < o.getX || target.getX + target.getWidth > o.getX + 5 ||
            target.getY + target.getHeight <= o.getY || target.getY >= o.getY + o.getHeight)
        }
        else false

      case HitStrategy.ABOVE => !(target.getX + target.getWidth < o.getX + 5 || target.getX > o.getX + o.getWidth - 5 ||
                                  target.getY < o.getY + o.getHeight || target.getY > o.getY + o.getHeight + 5)

      case HitStrategy.BACK => !(target.getX > o.getX + o.getWidth || target.getX + target.getWidth < o.getX + o.getWidth - 5 ||
                                 target.getY + target.getHeight <= o.getY || target.getY >= o.getY + o.getHeight)

      case HitStrategy.BELOW => !(target.getX + target.getWidth < o.getX + 5 || target.getX > o.getX + o.getWidth - 5 ||
                                  target.getY + target.getHeight < o.getY || (target.getY + target.getHeight) > o.getY + 5)

    }
  }
}