package model.characters.strategy

import model.GameObject
import model.characters.Character
import model.characters.strategy.HitStrategy.HitDirection

object HitStrategy extends Enumeration {
  type HitDirection = Value
  val AHEAD, BACK, ABOVE, BELOW = Value
}

trait HitStrategy {
  def hit(character: Character, target: GameObject, direction: HitDirection): Boolean
}