package model.characters.strategy

import game.Main
import model.characters.Mario

class SuperJumpStrategy extends JumpingStrategy {

  private val jumpLimit = 25
  private var jumpingExtent = 0

  override def performJump(target: Mario): Unit = {

    this.jumpingExtent = this.jumpingExtent + 1
    if (this.jumpingExtent < jumpLimit)
      if (target.getY > Main.getScene.getHeightLimit) target.setY(target.getY - 10) else this.jumpingExtent = jumpLimit
    else if (target.getY + target.getHeight < Main.getScene.getFloorOffsetY)
      target.setY(target.getY + 1)
    else {
      target.setJumping(false)
      this.jumpingExtent = 0
    }
  }
}