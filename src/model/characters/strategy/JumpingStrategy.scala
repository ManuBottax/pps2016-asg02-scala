package model.characters.strategy

import model.characters.Mario

trait JumpingStrategy {
  def performJump(target: Mario): Unit
}