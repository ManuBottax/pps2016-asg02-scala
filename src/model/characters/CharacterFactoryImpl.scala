package model.characters

class CharacterFactoryImpl extends CharacterFactory {

  private var marioCreated = false

  override def createMushroom(xPosition: Int, yPosition: Int) = Mushroom(xPosition, yPosition)

  override def createTurtle(xPosition: Int, yPosition: Int) = Turtle(xPosition, yPosition)

  override def createMario(xPosition: Int, yPosition: Int): Mario = if (!marioCreated) {
    marioCreated = true
    Mario(xPosition, yPosition)
  }
  else null
}