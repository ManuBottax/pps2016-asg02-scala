package model.objects

import utils.Res
import utils.Utils

object TunnelParameter {
  val WIDTH = 43
  val HEIGHT = 65
}

class Tunnel( x: Int,  y: Int) extends BaseElement(x, y, TunnelParameter.WIDTH, TunnelParameter.HEIGHT) {
  super.setObjectImage(Utils.getImage(Res.IMG_TUNNEL))
}