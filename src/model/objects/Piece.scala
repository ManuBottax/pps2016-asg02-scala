package model.objects

import utils._

import scala.swing.Image


object PieceParameter {
  val WIDTH: Int = 30
  val HEIGHT: Int = 30
}

class Piece( x: Int, y: Int) extends BaseElement(x, y, PieceParameter.WIDTH, PieceParameter.HEIGHT) with Runnable {

  private[this] val flipFrequency: Int = 100
  private var counter: Int = 0
  private val pause: Int = 10

  super.setObjectImage(Utils.getImage(Res.IMG_PIECE1))

  override def getObjectImage: Image = {
    this.counter = this.counter + 1
    this.counter match {
      case i if i % flipFrequency == 0 => Utils.getImage(Res.IMG_PIECE1)
      case _ => Utils.getImage(Res.IMG_PIECE2)
    }
  }

  def run() {

    while (true) {
      {
        this.getObjectImage
        try {
          Thread.sleep(pause)
        }
        catch {
          case e: InterruptedException => e.printStackTrace()
        }
      }
    }
  }
}