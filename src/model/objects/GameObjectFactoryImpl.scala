package model.objects

class GameObjectFactoryImpl extends GameObjectFactory {
  override def createBlock(xPosition: Int, yPosition: Int) = new Block(xPosition, yPosition)

  override def createTunnel(xPosition: Int, yPosition: Int) = new Tunnel(xPosition, yPosition)

  override def createPiece(xPosition: Int, yPosition: Int) = new Piece(xPosition, yPosition)
}