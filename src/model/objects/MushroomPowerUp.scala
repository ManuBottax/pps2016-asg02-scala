package model.objects

import utils._

object PowerUpParameter {
val WIDTH: Int = 30
val HEIGHT: Int = 30
}

class MushroomPowerUp( x: Int, y: Int) extends BaseElement(x, y, PowerUpParameter.WIDTH, PowerUpParameter.HEIGHT) {
  super.setObjectImage(Utils.getImage(Res.IMG_POWERUP))
}

