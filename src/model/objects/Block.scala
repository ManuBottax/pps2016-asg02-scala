package model.objects

import utils.Res
import utils.Utils

object BlockDimension {
  val WIDTH: Int = 30
  val HEIGHT: Int = 30
}

class Block( x: Int,  y: Int) extends BaseElement(x, y, BlockDimension.WIDTH, BlockDimension.HEIGHT) {
  super.setObjectImage(Utils.getImage(Res.IMG_BLOCK))
}