package model.objects

import java.awt.Image
import game.Main

class BaseElement (var x: Int, var y: Int, var width: Int, var height: Int) extends GameElement {
  private var objectImage: Image = _

  override def getWidth: Int = width

  def getHeight: Int =  height

  def getX: Int = x

  def getY: Int = y

  def getObjectImage: Image = this.objectImage

  def setObjectImage(image: Image) {
    this.objectImage = image
  }

  def move() {
    if (Main.scene.getXPos >= 0) {
      this.x = this.x - Main.scene.getMovement
    }
  }
}