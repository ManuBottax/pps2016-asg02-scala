package model.objects

trait GameObjectFactory {
  def createBlock(xPosition: Int, yPosition: Int): GameElement

  def createTunnel(xPosition: Int, yPosition: Int): GameElement

  def createPiece(xPosition: Int, yPosition: Int): GameElement
}