package model.objects

import model.GameObject
import scala.swing.Image

trait GameElement extends GameObject{

    def getObjectImage: Image
    def setObjectImage(image : Image)

}
