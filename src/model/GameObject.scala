package model

trait GameObject {
  def getWidth: Int

  def getHeight: Int

  def getX: Int

  def getY: Int

  def move()
}