package controller

import game.Main
import utils.Audio
import utils.Res
import java.awt.event.KeyEvent
import java.awt.event.KeyListener

class Keyboard extends KeyListener {

  val rightMove : Int = 1
  val leftMove : Int = -1

  private def checkMapEdge(): Unit = {
    val c: Int = Main.scene.getXPos
    c match {
      case -1 =>
        Main.scene.setXPos(0)
        setBackground()

      case 4601 =>
        Main.scene.setXPos(4600)
        setBackground()

      case _ => Unit
    }
  }

  private def setBackground(): Unit = {
    Main.scene.setBackground1PosX(-50)
    Main.scene.setBackground2PosX(750)
  }

  private def setMoving(rightDirection: Boolean) = {
    Main.scene.getMario.setMoving(true)
    Main.scene.getMario.setRightDirection(rightDirection)

    val c: Boolean = rightDirection
    c match {
      case true => Main.scene.setMovement(rightMove)
      case _ =>  Main.scene.setMovement(leftMove)
    }
  }

  override def keyPressed(e: KeyEvent): Unit =
    if (Main.scene.getMario.isAlive) {
      val c: Int = e.getKeyCode
      c match {

        case KeyEvent.VK_RIGHT =>
            checkMapEdge ()
            setMoving (true)

        case KeyEvent.VK_LEFT =>
            checkMapEdge ()
            setMoving (false)

        case KeyEvent.VK_UP =>
            Main.scene.getMario.setJumping (true)
            Audio.playSound (Res.AUDIO_JUMP)

        case _ => Unit
      }
    }

  override def keyReleased(e: KeyEvent): Unit = {
    Main.scene.getMario.setMoving(false)
    Main.scene.setMovement(0)
  }

  override def keyTyped(e: KeyEvent): Unit = {}
}